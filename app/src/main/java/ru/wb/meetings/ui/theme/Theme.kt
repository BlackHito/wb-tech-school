package ru.wb.meetings.ui.theme

 import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.graphics.Color

private val BaseAppScheme = AppColors(
    brandColorDark = Color(0xFF660EC8),
    brandColorDefault = Color(0xFF9A41FE),
    brandColorDarkMode = Color(0xFF8207E8),
    brandColorLight = Color(0xFFECDAFF),
    brandColorBG = Color(0xFFF5ECFF),

    neutralActive = Color(0xFF29183B),
    neutralDark = Color(0xFF190E26),
    neutralBody = Color(0xFF1D0835),
    neutralWeak = Color(0xFFA4A4A4),
    neutralDisable = Color(0xFFADB5BD),
    neutralLine = Color(0xFFEDEDED),
    neutralSecondaryBG = Color(0xFFF7F7FC),
    neutralWhite = Color(0xFFFFFFFF),

    accentDanger = Color(0xFFE94242),
    accentWarning = Color(0xFFFDCF41),
    accentSuccess = Color(0xFF2CC069),
    accentSafe = Color(0xFF7BCBCF)
)

@Composable
fun MeetTheme(
    content: @Composable () -> Unit
) {
    val colorScheme = BaseAppScheme

    CompositionLocalProvider(
        LocalAppTypography provides appTypography,
        LocalAppColors provides colorScheme,
        content = content
    )
}