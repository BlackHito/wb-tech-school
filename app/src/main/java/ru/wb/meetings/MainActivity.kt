package ru.wb.meetings

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import ru.wb.meetings.components.base.BaseAvatars
import ru.wb.meetings.components.base.BaseButtons
import ru.wb.meetings.components.base.BaseChips
import ru.wb.meetings.components.base.BaseSearchBar
import ru.wb.meetings.components.base.BaseTypography
import ru.wb.meetings.components.events.OverlappingVisitorsList
import ru.wb.meetings.components.profile.ProfileAvatarAdd
import ru.wb.meetings.ui.theme.MeetTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            MeetTheme {
                    Scaffold(modifier = Modifier.fillMaxSize()) { innerPadding ->
                        Showcase(
                            modifier = Modifier.padding(innerPadding)
                        )
                    }
            }
        }
    }
}

@Composable
fun Showcase(modifier: Modifier = Modifier) {
    LazyColumn(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        content = {
            item {
                BaseButtons()
            }
            item {
                BaseTypography()
            }
            item {
                BaseAvatars()
            }
            item {
                BaseSearchBar()
            }
            item {
                BaseChips()
            }

            item {
                val listImages = listOf(
                    R.drawable.icon_user_example,
                    R.drawable.icon_community,
                    R.drawable.icon_meeting,
                    R.drawable.icon_user_example,
                    R.drawable.icon_user_example,
                    R.drawable.icon_user_example,
                    R.drawable.icon_user_example,
                    R.drawable.icon_user_example,
                    R.drawable.icon_user_example,
                    R.drawable.icon_user_example,
                    R.drawable.icon_user_example,
                    R.drawable.icon_user_example
                )
                OverlappingVisitorsList(listImages, visibleLimit = 5)
            }

            item {
                ProfileAvatarAdd(painter = painterResource(id = R.drawable.variant_user), isBaseAvatar = true)
            }

            item {
                ProfileAvatarAdd(painter = painterResource(id = R.drawable.icon_user_example))
            }
        }
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    MeetTheme {
        Showcase()
    }
}