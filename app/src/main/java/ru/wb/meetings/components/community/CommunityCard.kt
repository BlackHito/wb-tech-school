package ru.wb.meetings.components.community

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.wb.meetings.R
import ru.wb.meetings.components.base.SquareAvatar
import ru.wb.meetings.ui.theme.MeetTheme

@Composable
fun CommunityCard(modifier: Modifier = Modifier, community: Community) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(),
                onClick = {})
    ) {
        SquareAvatar(
            Modifier.padding(end = 12.dp),
            painter = painterResource(id = community.avatarResourceId)
        )
        Column(modifier = Modifier.fillMaxWidth()) {
            Text(
                modifier = Modifier.padding(top = 4.dp),
                text = community.name,
                style = MeetTheme.typography.body1,
                color = MeetTheme.colors.neutralActive
            )
            Text(
                modifier = Modifier.padding(top = 2.dp),
                text = community.sizeOfCommunity.toString()
                    .reversed()
                    .chunked(3)
                    .joinToString(" ")
                    .reversed()
                    .plus(" человек"),
                style = MeetTheme.typography.metadata1,
                color = MeetTheme.colors.neutralDisable
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun CommunityCardPreview() {
    MeetTheme {
        CommunityCard(
            community = Community(
                name = "Designa",
                sizeOfCommunity = 10_000,
                avatarResourceId = R.drawable.icon_community
            )
        )
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun CommunityCardListPreview() {
    MeetTheme {
        val listOfCommunity = listOf(
            Community(
                name = "Designa",
                sizeOfCommunity = 10_000,
                avatarResourceId = R.drawable.icon_community
            ),
            Community(
                name = "Testita",
                sizeOfCommunity = 10_000,
                avatarResourceId = R.drawable.icon_community
            ),
            Community(
                name = "Cats_In_IT",
                sizeOfCommunity = 100_000,
                avatarResourceId = R.drawable.icon_community
            ),
            Community(
                name = "Mobile developer",
                sizeOfCommunity = 30_000,
                avatarResourceId = R.drawable.icon_community
            ),
            Community(
                name = "Dev_Rel",
                sizeOfCommunity = 1_000,
                avatarResourceId = R.drawable.icon_community
            ),
            Community(
                name = "KOTLIN",
                sizeOfCommunity = 500,
                avatarResourceId = R.drawable.icon_community
            )
        )

        LazyColumn(
            Modifier
                .fillMaxWidth()
                .padding(horizontal = 24.dp)
        ) {
            itemsIndexed(listOfCommunity) { index, community ->
                CommunityCard(modifier = Modifier.padding(top = 16.dp), community = community)
                if (index != listOfCommunity.lastIndex)
                    HorizontalDivider(Modifier.padding(top = 12.dp), color = MeetTheme.colors.neutralLine)
            }
        }
    }
}