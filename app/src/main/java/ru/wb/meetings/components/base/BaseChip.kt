package ru.wb.meetings.components.base

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.wb.meetings.ui.theme.MeetTheme

@Composable
fun BaseChips() {
    Row(horizontalArrangement = Arrangement.spacedBy(4.dp)) {
        BaseChip(text = "Python")
        BaseChip(text = "Junior")
        BaseChip(text = "Moscow")
    }
}

@Composable
fun BaseChip(text: String, modifier: Modifier = Modifier) {
    Box(
        modifier = modifier
            .background(color = MeetTheme.colors.brandColorBG, shape = RoundedCornerShape(40.dp))
            .padding(horizontal = 8.dp, vertical = 4.dp)

    ) {
        Text(
            text = text,
            style = MeetTheme.typography.metadata3,
            color = MeetTheme.colors.brandColorDark
        )
    }

}

@Preview(showBackground = true)
@Composable
private fun TestChipsPreview() {
    MeetTheme {
        BaseChips()
    }
}