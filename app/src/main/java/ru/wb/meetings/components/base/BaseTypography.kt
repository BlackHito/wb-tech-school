package ru.wb.meetings.components.base

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.wb.meetings.ui.theme.MeetTheme

@Composable
fun BaseTypography() {
    Column {
        TypographyElement(
            name = "Heading 1",
            subText = "SF Pro Display/32/Bold",
            style = MeetTheme.typography.heading1
        )
        TypographyElement(
            name = "Heading 2",
            subText = "SF Pro Display/24/Bold",
            style = MeetTheme.typography.heading2
        )
        TypographyElement(
            name = "Subheading 1",
            subText = "SF Pro Display/18/SemiBold",
            style = MeetTheme.typography.subheading1
        )
        TypographyElement(
            name = "Subheading 2",
            subText = "SF Pro Display/16/SemiBold",
            style = MeetTheme.typography.subheading2
        )
        TypographyElement(
            name = "Body Text 1",
            subText = "SF Pro Display/14/SemiBold",
            style = MeetTheme.typography.body1
        )
        TypographyElement(
            name = "Body Text 2",
            subText = "SF Pro Display14/Regular",
            style = MeetTheme.typography.body2
        )
        TypographyElement(
            name = "Metadata 1",
            subText = "SF Pro Display12/Regular",
            style = MeetTheme.typography.metadata1
        )
        TypographyElement(
            name = "Metadata 2",
            subText = "SF Pro Display10/Regular",
            style = MeetTheme.typography.metadata2
        )
        TypographyElement(
            name = "Metadata 3",
            subText = "SF Pro Display10/SemiBold",
            style = MeetTheme.typography.metadata3
        )
    }
}

@Composable
private fun TypographyElement(name: String, subText: String, style: TextStyle) {
    Row(
        modifier = Modifier
            .padding(16.dp)
            .sizeIn(minHeight = 64.dp)
    ) {
        Column(
            modifier = Modifier
                .padding(end = 16.dp)
                .widthIn(200.dp),
            verticalArrangement = Arrangement.spacedBy(4.dp)
        ) {
            Text(
                text = name,
                style = MeetTheme.typography.subheading1,
            )

            Text(
                color = MeetTheme.colors.neutralDisable,
                style = MeetTheme.typography.body2,
                text = subText,
            )
        }
        Text(
            style = style,
            text = "The quick brown fox jumps over the lazy dog",
        )
    }

}

@Preview(showBackground = true)
@Composable
fun TestTypographyPreview() {
    MeetTheme {
        BaseTypography()
    }
}