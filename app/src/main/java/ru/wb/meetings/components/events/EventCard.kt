package ru.wb.meetings.components.events

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.wb.meetings.R
import ru.wb.meetings.components.base.BaseChip
import ru.wb.meetings.components.base.SquareAvatar
import ru.wb.meetings.ui.theme.MeetTheme

@Composable
fun EventCard(modifier: Modifier = Modifier, event: Event) {
    Row(
        modifier = modifier
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(),
                onClick = {})
            .fillMaxWidth()
            .padding()
    ) {
        SquareAvatar(
            Modifier.padding(end = 12.dp),
            painter = painterResource(id = event.avatarResourceId)
        )
        Column(modifier = Modifier.fillMaxWidth()) {
            EventTitle(event = event)
            Text(
                modifier = Modifier.padding(top = 2.dp),
                text = "${event.date} — ${event.location}",
                style = MeetTheme.typography.metadata1,
                color = MeetTheme.colors.neutralWeak
            )
            TagsList(event.tags)
        }
    }
}

@Composable
private fun EventTitle(event: Event) {
    Box(modifier = Modifier.fillMaxWidth()) {
        Text(
            text = event.name,
            style = MeetTheme.typography.body1,
            color = MeetTheme.colors.neutralActive
        )

        if (event.isEventEnded) {
            Text(
                modifier = Modifier.align(Alignment.TopEnd),
                style = MeetTheme.typography.metadata2,
                color = MeetTheme.colors.neutralWeak,
                text = stringResource(id = R.string.event_ended)
            )
        }
    }

}

@OptIn(ExperimentalLayoutApi::class)
@Composable
private fun TagsList(tags: List<String>) {
    FlowRow {
        tags.forEach { tag ->
            BaseChip(text = tag, modifier = Modifier.padding(top = 4.dp, bottom = 4.dp, end = 4.dp))
        }
    }
}

@Preview(showBackground = true)
@Composable
fun EventCardPreview() {
    MeetTheme {
        EventCard(
            event = Event(
                name = "Developer meeting",
                date = "13.09.2024",
                location = "Moscow",
                isEventEnded = false,
                tags = listOf("Python", "Android")
            )
        )
    }
}

@Preview(showSystemUi = true, showBackground = true)
@Composable
fun EventCardListPreview() {
    MeetTheme {
        val eventList = listOf(
            Event(
                name = "Python meeting",
                date = "2024-12-12",
                location = "Moscow",
                tags = listOf("Kotlin", "Android", "Web3", "OpenAI", "iOS", "AI", "KMP"),
                isEventEnded = false,
                avatarResourceId = R.drawable.icon_meeting
            ),

            Event(
                name = "MOBUIS",
                date = "2024-5-1",
                location = "Saint-Petersburg",
                tags = listOf("iOS", "AI"),
                isEventEnded = false,
                avatarResourceId = R.drawable.icon_meeting
            ),

            Event(
                name = "IT meeting ALL cities",
                date = "2024-12-12",
                location = "Moscow",
                tags = listOf("Kotlin", "Android", "Web3", "OpenAI", "iOS", "AI"),
                isEventEnded = true,
                avatarResourceId = R.drawable.icon_meeting
            ),

            Event(
                name = "Meetup Mobile developers",
                date = "2024-6-22",
                location = "Saransk",
                tags = listOf("Kotlin", "Android", "iOS", "Swift"),
                isEventEnded = false,
                avatarResourceId = R.drawable.icon_meeting
            ),

            Event(
                name = "Coffee&Code",
                date = "2024-10-12",
                location = "Moscow",
                tags = listOf("Kotlin", "Android", "Web3", "OpenAI", "iOS", "AI"),
                isEventEnded = true,
                avatarResourceId = R.drawable.icon_meeting
            )
        )

        LazyColumn(modifier = Modifier.padding(horizontal = 24.dp)) {
            itemsIndexed(eventList) { index, event ->
                EventCard(modifier = Modifier.padding(top = 16.dp), event = event)
                if (index != eventList.lastIndex)
                    HorizontalDivider(
                        modifier = Modifier.padding(top = 12.dp),
                        color = MeetTheme.colors.neutralLine
                    )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun EventCardPreview3() {
    MeetTheme {
        EventCard(
            event = Event(
                name = "Coffee&Code",
                date = "2024-10-12",
                location = "Moscow",
                tags = listOf("Kotlin", "Android", "Web3", "OpenAI", "iOS", "AI"),
                isEventEnded = true,
                avatarResourceId = R.drawable.icon_meeting
            )
        )
    }
}