package ru.wb.meetings.components.events

import ru.wb.meetings.R

data class Event(
    val name: String,
    val date: String,
    val location: String,
    val tags: List<String>,
    val isEventEnded: Boolean = false,
    val avatarResourceId: Int = R.drawable.icon_meeting
)