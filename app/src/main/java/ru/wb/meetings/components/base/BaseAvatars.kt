package ru.wb.meetings.components.base

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.wb.meetings.R
import ru.wb.meetings.ui.theme.MeetTheme

@Composable
fun BaseAvatars() {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        BaseProfileAvatar(painter = painterResource(id = R.drawable.icon_user_example), isBaseAvatar = false)
        BaseProfileAvatar(isBaseAvatar = true)
        SquareAvatar(painter = painterResource(id = R.drawable.icon_meeting))
    }
}

@Composable
fun BaseProfileAvatar(
    modifier: Modifier = Modifier,
    painter: Painter = painterResource(id = R.drawable.variant_user),
    isBaseAvatar: Boolean = false
) {
    Box(
        modifier = modifier
            .width(200.dp)
            .height(200.dp)
            .background(color = MeetTheme.colors.neutralWhite, shape = CircleShape),
    ) {
        Image(
            modifier = Modifier
                .align(alignment = Alignment.Center)
                .clip(CircleShape)
                .fillMaxSize()
                .padding(if (isBaseAvatar) 48.dp else 0.dp),
            contentScale = ContentScale.FillWidth,
            painter = painter, contentDescription = "",
        )
    }
}

@Composable
fun SquareAvatar(modifier: Modifier = Modifier, painter: Painter) {
    Image(
        modifier = modifier
            .padding(4.dp)
            .size(48.dp)
            .clip(RoundedCornerShape(16.dp)),
        contentScale = ContentScale.Crop,
        painter = painter, contentDescription = ""
    )
}

@Preview(showBackground = false)
@Composable
fun TestAvatarsPreview() {
    MeetTheme {
        BaseAvatars()
    }
}