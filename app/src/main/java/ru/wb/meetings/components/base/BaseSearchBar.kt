package ru.wb.meetings.components.base

import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsFocusedAsState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Search
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.wb.meetings.ui.theme.MeetTheme

@Composable
fun BaseSearchBar() {
    var search by remember { mutableStateOf("") }
    BaseSearchBar(search = search, onValueChange = {
        search = it
    })
}

@Composable
fun BaseSearchBar(
    search: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    val interactionSource = remember { MutableInteractionSource() }
    val isFocused by interactionSource.collectIsFocusedAsState()
    val currentColor =
        if (isFocused) MeetTheme.colors.neutralActive else MeetTheme.colors.neutralDisable
    OutlinedTextField(
        modifier = modifier,
        interactionSource = interactionSource,
        value = search,
        onValueChange = onValueChange,
        placeholder = { Text(color = currentColor, text = "Поиск") },
        singleLine = true,
        leadingIcon = {
            Icon(
                imageVector = Icons.Rounded.Search,
                tint = currentColor,
                contentDescription = "Поиск"
            )
        },
        textStyle = MeetTheme.typography.body1,
        colors = OutlinedTextFieldDefaults.colors(
            focusedContainerColor = MeetTheme.colors.neutralSecondaryBG,
            unfocusedContainerColor = MeetTheme.colors.neutralSecondaryBG,
            unfocusedBorderColor = Color.Transparent,
            focusedBorderColor = MeetTheme.colors.neutralLine
        ),
        shape = RoundedCornerShape(4.dp)
    )
}

@Preview(showBackground = false, showSystemUi = false)
@Composable
fun TestSearchBarPreview() {
    MeetTheme {
        BaseSearchBar()
    }
}