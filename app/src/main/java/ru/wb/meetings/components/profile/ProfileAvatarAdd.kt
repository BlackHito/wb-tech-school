package ru.wb.meetings.components.profile

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.wb.meetings.R
import ru.wb.meetings.components.base.BaseProfileAvatar
import ru.wb.meetings.ui.theme.MeetTheme

@Composable
fun ProfileAvatarAdd( painter: Painter, modifier: Modifier = Modifier, isBaseAvatar: Boolean = false) {
    Box(modifier = modifier) {
        BaseProfileAvatar(painter = painter, isBaseAvatar = isBaseAvatar)
        Icon(
            modifier = Modifier
                .size(48.dp)
                .align(alignment = Alignment.BottomEnd)
                .background(color = MeetTheme.colors.neutralActive, shape = CircleShape),
            imageVector = Icons.Rounded.Add, contentDescription = "Badge",
            tint = MeetTheme.colors.neutralWhite
        )
    }
}

@Preview(showBackground = false)
@Composable
fun ProfileAvatarAddPreview() {
    MeetTheme {
        ProfileAvatarAdd(painter = painterResource(id = R.drawable.variant_user), isBaseAvatar = true)
    }
}