package ru.wb.meetings.components.base

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.hoverable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.wb.meetings.ui.theme.MeetTheme

@Composable
fun BaseButtons() {
    Column {
        ButtonRow()
        ButtonRow()
        ButtonRow(enabled = false)
    }
}

@Composable
private fun ButtonRow(
    enabled: Boolean = true
) {
    Row(
        modifier = Modifier
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        BaseButton(onClick = {}, enabled = enabled)
        BaseOutlinedButton(onClick = {}, enabled = enabled)
        BaseTextButton(onClick = {}, enabled = enabled)
    }
}


@Composable
fun BaseButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    text: String = "Button"
) {
    val interactionSource = remember { MutableInteractionSource() }
    val isPressed by interactionSource.collectIsPressedAsState()

    Button(
        modifier = modifier
            .hoverable(interactionSource = interactionSource)
            .alpha(if (enabled) 1.0f else 0.5f),
        onClick = onClick,
        enabled = enabled,
        colors = if (isPressed)
            ButtonDefaults.buttonColors(
                containerColor = MeetTheme.colors.brandColorDark,
                disabledContainerColor = MeetTheme.colors.brandColorDark,
                disabledContentColor = MeetTheme.colors.neutralWhite
            )
        else
            ButtonDefaults.buttonColors(
                containerColor = MeetTheme.colors.brandColorDefault,
                disabledContainerColor = MeetTheme.colors.brandColorDark,
                disabledContentColor = MeetTheme.colors.neutralWhite
            )

    ) {
        Text(text = text)
    }
}

@Composable
fun BaseOutlinedButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    text: String = "Button"
) {
    val interactionSource = remember { MutableInteractionSource() }
    val isPressed by interactionSource.collectIsPressedAsState()

    OutlinedButton(
        modifier = modifier
            .hoverable(interactionSource = interactionSource)
            .alpha(if (enabled) 1.0f else 0.5f),
        onClick = onClick,
        enabled = enabled,
        border = BorderStroke(1.dp, MeetTheme.colors.brandColorDark),
        colors = if (isPressed)
            ButtonDefaults.outlinedButtonColors(
                contentColor = MeetTheme.colors.brandColorDark,
                containerColor = Color.Transparent,
                disabledContainerColor = Color.Transparent,
                disabledContentColor = MeetTheme.colors.brandColorDefault
            )
        else
            ButtonDefaults.outlinedButtonColors(
                contentColor = MeetTheme.colors.brandColorDefault,
                containerColor = Color.Transparent,
                disabledContainerColor = Color.Transparent,
                disabledContentColor = MeetTheme.colors.brandColorDefault
            )

    ) {
        Text(text = text)
    }
}

@Composable
fun BaseTextButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    text: String = "Button"
) {
    val interactionSource = remember { MutableInteractionSource() }
    val isPressed by interactionSource.collectIsPressedAsState()

    TextButton(
        modifier = modifier
            .hoverable(interactionSource = interactionSource)
            .alpha(if (enabled) 1.0f else 0.5f),
        onClick = onClick,
        enabled = enabled,
        colors = if (isPressed)
            ButtonDefaults.textButtonColors(
                contentColor = MeetTheme.colors.brandColorDark,
                containerColor = Color.Transparent,
                disabledContainerColor = Color.Transparent,
                disabledContentColor = MeetTheme.colors.brandColorDefault
            )
        else
            ButtonDefaults.textButtonColors(
                contentColor = MeetTheme.colors.brandColorDefault,
                containerColor = Color.Transparent,
                disabledContainerColor = Color.Transparent,
                disabledContentColor = MeetTheme.colors.brandColorDefault
            )

    ) {
        Text(text = text)
    }
}

@Preview(showBackground = true)
@Composable
fun TestButtonsPreview() {
    MeetTheme {
        BaseButtons()
    }
}