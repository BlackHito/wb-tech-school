package ru.wb.meetings.components.events

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.wb.meetings.R
import ru.wb.meetings.components.base.SquareAvatar
import ru.wb.meetings.ui.theme.MeetTheme

@Composable
fun OverlappingVisitorsList(avatarsList: List<Int>, visibleLimit: Int) {
    Row(Modifier.fillMaxWidth()) {
        if (avatarsList.isNotEmpty()) {
            OverlappingRow(overlappingPercentage = 0.30f) {
                avatarsList.take(visibleLimit).forEach {
                    SquareAvatar(
                        modifier = Modifier
                            .border(2.dp, Color(0xFFD2D5F9), shape = RoundedCornerShape(20.dp))
                            .size(56.dp),
                        painter = painterResource(id = it)
                    )
                }
            }

            if (avatarsList.size > visibleLimit)
                Text(
                    modifier = Modifier
                        .align(alignment = Alignment.CenterVertically)
                        .padding(start = 10.dp), text = "+${avatarsList.size - visibleLimit}",
                    style = MeetTheme.typography.body1
                )
        }
    }
}

@Composable
private fun OverlappingRow(
    modifier: Modifier = Modifier,
    overlappingPercentage: Float,
    content: @Composable () -> Unit
) {
    val factor = (1 - overlappingPercentage)

    Layout(modifier = modifier, content = content, measurePolicy = { measurables, constraints ->
        val placeables = measurables.map { it.measure(constraints) }
        val widthsExceptFirst = placeables.subList(1, placeables.size).sumOf { it.width }
        val firstWidth = placeables.getOrNull(0)?.width ?: 0
        val width = (widthsExceptFirst * factor + firstWidth).toInt()
        val height = placeables.maxOf { it.height }
        layout(width, height) {
            var x = 0
            placeables.forEachIndexed { index, placeable ->
                placeable.placeRelative(x, 0, (placeables.size - index).toFloat())
                x += (placeable.width * factor).toInt()
            }
        }
    })
}


@Preview(showBackground = true)
@Composable
fun OverlappingVisitorsListPreview() {
    MeetTheme {
        val listImages = listOf(
            R.drawable.icon_user_example,
            R.drawable.icon_community,
            R.drawable.icon_meeting,
            R.drawable.icon_user_example,
            R.drawable.icon_user_example,
            R.drawable.icon_user_example,
            R.drawable.icon_user_example,
            R.drawable.icon_user_example,
            R.drawable.icon_user_example,
            R.drawable.icon_user_example,
            R.drawable.icon_user_example,
            R.drawable.icon_user_example
        )
        OverlappingVisitorsList(listImages, visibleLimit = 5)
    }
}