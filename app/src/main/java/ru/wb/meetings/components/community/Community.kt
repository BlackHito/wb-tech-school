package ru.wb.meetings.components.community

import ru.wb.meetings.R

data class Community(
    val name: String,
    val sizeOfCommunity: Int,
    val avatarResourceId: Int = R.drawable.icon_meeting
)